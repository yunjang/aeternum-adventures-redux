var witherBone = <netherex:wither_bone>;
var skeletonBone = <minecraft:bone>;
var arrow = <minecraft:arrow>;
var ribBone = <xreliquary:mob_ingredient>;
var spiderString = <minecraft:string>;
var spiderEye = <minecraft:spider_eye>;
var gunpowder = <minecraft:gunpowder>;
var cactusGreen = <minecraft:dye:2>;
var feather = <minecraft:feather>;
var rottenFlesh = <minecraft:rotten_flesh>;
var blazeRod = <minecraft:blaze_rod>;
var magmaCream = <minecraft:magma_cream>;
var enderPearl = <minecraft:ender_pearl>;
var leather = <minecraft:leather>;

var slimeBall = <minecraft:slime_ball>;
var slimeBallBlue = <tconstruct:edible:1>;
var slimeBallPurple = <tconstruct:edible:2>;
var slimeBallRed = <tconstruct:edible:4>;

var witherRibBone = <xreliquary:mob_ingredient:1>;
var chelicerae = <xreliquary:mob_ingredient:2>;
var catalyzingGland = <xreliquary:mob_ingredient:3>;
var slimePearl = <xreliquary:mob_ingredient:4>;
var batWing = <xreliquary:mob_ingredient:5>;
var zombieHeart = <xreliquary:mob_ingredient:6>;
var moltenCore = <xreliquary:mob_ingredient:7>;
var nebulousHeart = <xreliquary:mob_ingredient:11>;

// Rib Bone Recipe
mods.jei.JEI.addDescription(ribBone, "[B] = Skeleton Bone", "[A] = Arrow", "", "[B] [B] [B]", "[B] [A] [B]", "[B] [B] [B]");
recipes.addShaped("eidolonRibBone", ribBone, [[skeletonBone, skeletonBone, skeletonBone], [skeletonBone, arrow, skeletonBone], [skeletonBone, skeletonBone, skeletonBone]]);

// Wither Rib Bone Recipe
mods.jei.JEI.addDescription(witherRibBone, "[W] = Wither Rib Bone", "[B] = Skeleton Bone", "", "[W] [W] [W]", "[B] [ ] [B]", "[W] [W] [W]");
recipes.addShaped("eidolonWitherRibBone", witherRibBone, [[witherBone, witherBone, witherBone], [skeletonBone, null, skeletonBone], [witherBone, witherBone, witherBone]]);

// Chelicerae Recipe
mods.jei.JEI.addDescription(chelicerae, "[S] = String", "[E] = Spider Eye", "", "[S] [S] [S]", "[S] [E] [S]", "[S] [S] [S]");
recipes.addShaped("eidolonChelicerae", chelicerae, [[spiderString, spiderString, spiderString], [spiderString, spiderEye, spiderString], [spiderString, spiderString, spiderString]]);

// Catalyzing Gland Recipe
mods.jei.JEI.addDescription(catalyzingGland, "[G] = Gunpowder", "[C] = Cactus Green", "", "[G] [G] [G]", "[G] [C] [G]", "[G] [G] [G]");
recipes.addShaped("eidolonCatalyzingGland", catalyzingGland, [[gunpowder, gunpowder, gunpowder], [gunpowder, cactusGreen, gunpowder], [gunpowder, gunpowder, gunpowder]]);

// Slime Pearl Recipe
mods.jei.JEI.addDescription(slimePearl, "[S] = Slime Ball", "[C] = Cactus Green", "", "[S] [S] [S]", "[S] [C] [S]", "[S] [S] [S]");
recipes.addShaped("eidolonSlimePearl", slimePearl, [[slimeBall, slimeBall, slimeBall], [slimeBall, cactusGreen, slimeBall], [slimeBall, slimeBall, slimeBall]]);
recipes.addShaped("eidolonSlimePearlBlue", slimePearl, [[slimeBallBlue, slimeBallBlue, slimeBallBlue], [slimeBallBlue, cactusGreen, slimeBallBlue], [slimeBallBlue, slimeBallBlue, slimeBallBlue]]);
recipes.addShaped("eidolonSlimePearlPurple", slimePearl, [[slimeBallPurple, slimeBallPurple, slimeBallPurple], [slimeBallPurple, cactusGreen, slimeBallPurple], [slimeBallPurple, slimeBallPurple, slimeBallPurple]]);
recipes.addShaped("eidolonSlimePearlRed", slimePearl, [[slimeBallRed, slimeBallRed, slimeBallRed], [slimeBallRed, cactusGreen, slimeBallRed], [slimeBallRed, slimeBallRed, slimeBallRed]]);

// Bat Wing Recipe
mods.jei.JEI.addDescription(batWing, "[F] = Feather", "[R] = Rotten Flesh", "", "[F] [F] [F]", "[F] [R] [F]", "[F] [F] [F]");
recipes.addShaped("eidolonBatWing", batWing, [[feather, feather, feather], [feather, rottenFlesh, feather], [feather, feather, feather]]);

// Zombie Heart Recipe
mods.jei.JEI.addDescription(zombieHeart, "[R] = Rotten Flesh", "[S] = Skeleton Bone", "", "[R] [R] [R]", "[R] [S] [R]", "[R] [R] [R]");
recipes.addShaped("eidolonZombieHeart", zombieHeart, [[rottenFlesh, rottenFlesh, rottenFlesh], [rottenFlesh, skeletonBone, rottenFlesh], [rottenFlesh, rottenFlesh, rottenFlesh]]);

// Molten Core Recipe
mods.jei.JEI.addDescription(moltenCore, "[B] = Blaze Rod", "[M] = Magma Cream", "", "[B] [B] [B]", "[M] [M] [M]", "[B] [B] [B]");
recipes.addShaped("eidolonMoltenCore", moltenCore, [[blazeRod, blazeRod, blazeRod], [magmaCream, magmaCream, magmaCream], [blazeRod, blazeRod, blazeRod]]);

// Nebulous Heart Recipe
mods.jei.JEI.addDescription(nebulousHeart, "[E] = Ender Pearl", "[L] = Leather", "", "[E] [E] [E]", "[E] [L] [E]", "[E] [E] [E]");
recipes.addShaped("eidolonNebulousHeart", nebulousHeart, [[enderPearl, enderPearl, enderPearl], [enderPearl, leather, enderPearl], [enderPearl, enderPearl, enderPearl]]);